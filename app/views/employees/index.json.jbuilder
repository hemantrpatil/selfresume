json.array!(@employees) do |employee|
  json.extract! employee, :id, :first_name, :middle_name, :last_name, :email, :age, :date_of_birth
  json.url employee_url(employee, format: :json)
end
