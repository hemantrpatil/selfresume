json.array!(@tech_skills) do |tech_skill|
  json.extract! tech_skill, :id, :skill_name, :presentage, :institute_name, :start_date, :end_date, :user_id
  json.url tech_skill_url(tech_skill, format: :json)
end
