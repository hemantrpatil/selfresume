json.array!(@experiences) do |experience|
  json.extract! experience, :id, :companyname, :designation, :companyprofile, :duration
  json.url experience_url(experience, format: :json)
end
