json.array!(@educations) do |education|
  json.extract! education, :id, :course, :institute, :university/board, :year_of_passing
  json.url education_url(education, format: :json)
end
