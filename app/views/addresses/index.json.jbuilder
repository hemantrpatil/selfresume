json.array!(@addresses) do |address|
  json.extract! address, :id, :address, :city, :state, :zipcode, :mobileno, :landlineno
  json.url address_url(address, format: :json)
end
