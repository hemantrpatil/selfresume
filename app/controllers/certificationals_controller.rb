class CertificationalsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_certificational, only: [:show, :edit, :update, :destroy]

  # GET /certificationals
  # GET /certificationals.json
  def index
    @certificationals = current_user.certificationals
  end

  # GET /certificationals/1
  # GET /certificationals/1.json
  def show
  end

  # GET /certificationals/new
  def new
    @certificational = Certificational.new
  end

  # GET /certificationals/1/edit
  def edit
  end

  # POST /certificationals
  # POST /certificationals.json
  def create
    @certificational = Certificational.new(certificational_params)
    @certificational.user_id = current_user.id    

    respond_to do |format|
      if @certificational.save
        format.html { redirect_to @certificational, notice: 'Certificational was successfully created.' }
        format.json { render :show, status: :created, location: @certificational }
      else
        format.html { render :new }
        format.json { render json: @certificational.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /certificationals/1
  # PATCH/PUT /certificationals/1.json
  def update
    respond_to do |format|
      if @certificational.update(certificational_params)
        format.html { redirect_to @certificational, notice: 'Certificational was successfully updated.' }
        format.json { render :show, status: :ok, location: @certificational }
      else
        format.html { render :edit }
        format.json { render json: @certificational.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /certificationals/1
  # DELETE /certificationals/1.json
  def destroy
    @certificational.destroy
    respond_to do |format|
      format.html { redirect_to certificationals_url, notice: 'Certificational was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_certificational
      @certificational = Certificational.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def certificational_params
      params.require(:certificational).permit(:certification_name, :presentage, :institute_name, :start_date, :end_date, :user_id)
    end
end
