class TechSkillsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_tech_skill, only: [:show, :edit, :update, :destroy]

  # GET /tech_skills
  # GET /tech_skills.json
  def index

    

    @tech_skills = TechSkill.all
  end

  # GET /tech_skills/1
  # GET /tech_skills/1.json
  def show
  end

  # GET /tech_skills/new
  def new
    @tech_skill = TechSkill.new
  end

  # GET /tech_skills/1/edit
  def edit
  end

  # POST /tech_skills
  # POST /tech_skills.json
  def create

    
    @tech_skill = TechSkill.new(tech_skill_params)
    @tech_skill.user_id = current_user.id
   

    respond_to do |format|
      if @tech_skill.save
        format.html { redirect_to @tech_skill, notice: 'Tech skill was successfully created.' }
        format.json { render :show, status: :created, location: @tech_skill }
      else
        format.html { render :new }
        format.json { render json: @tech_skill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tech_skills/1
  # PATCH/PUT /tech_skills/1.json
  def update
    respond_to do |format|
      if @tech_skill.update(tech_skill_params)
        format.html { redirect_to @tech_skill, notice: 'Tech skill was successfully updated.' }
        format.json { render :show, status: :ok, location: @tech_skill }
      else
        format.html { render :edit }
        format.json { render json: @tech_skill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tech_skills/1
  # DELETE /tech_skills/1.json
  def destroy
    @tech_skill.destroy
    respond_to do |format|
      format.html { redirect_to tech_skills_url, notice: 'Tech skill was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tech_skill
      @tech_skill = TechSkill.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tech_skill_params
      params.require(:tech_skill).permit(:skill_name, :presentage, :institute_name, :start_date, :end_date, :user_id)
    end
end
