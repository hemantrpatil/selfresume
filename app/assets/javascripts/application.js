// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
 

//= require jquery
// require jquery_ujs
// require turbolinks
// require_tree .

//= require "plugins/jquery.min.js"
//= require "plugins/modernizr.min.js"
//= require "plugins/bootstrap.min.js"
//= require "plugins/enscroll.min.js"
//= require "plugins/footable.min.js"
//= require "plugins/knob.min.js"
//= require "plugins/sparkline.min.js"
//= require "plugins/rickshaw/d3.v3.js"
// require "plugins/rickshaw/rickshaw.min.js"
//= require "plugins/flot.min.js"
//= require "plugins/icheck.min.js"
// require "page-dashboard.js"
//= require "main.js"
//= require ckeditor/init
//= require bootstrap.js
//= require bootstrap.min.js
//= require script.js