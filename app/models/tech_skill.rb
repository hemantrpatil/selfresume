# == Schema Information
#
# Table name: tech_skills
#
#  id             :integer          not null, primary key
#  skill_name     :string(255)
#  presentage     :integer
#  institute_name :string(255)
#  start_date     :date
#  end_date       :date
#  created_at     :datetime
#  updated_at     :datetime
#  user_id        :integer
#

class TechSkill < ActiveRecord::Base
	belongs_to :employee
	belongs_to :users
end
