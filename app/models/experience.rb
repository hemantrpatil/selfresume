# == Schema Information
#
# Table name: experiences
#
#  id             :integer          not null, primary key
#  companyname    :string(255)
#  designation    :string(255)
#  companyprofile :string(255)
#  duration       :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  user_id        :integer
#

class Experience < ActiveRecord::Base
	belongs_to :employee
	belongs_to :users
end
