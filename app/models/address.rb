# == Schema Information
#
# Table name: addresses
#
#  id           :integer          not null, primary key
#  address      :string(255)
#  city         :string(255)
#  state        :string(255)
#  zipcode      :string(255)
#  mobileno     :string(255)
#  landlineno   :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#  employee_id  :integer
#  address_p    :string(255)
#  city_p       :string(255)
#  state_p      :string(255)
#  zipcode_p    :string(255)
#  mobileno_p   :string(255)
#  landlineno_p :string(255)
#  user_id      :integer
#



class Address < ActiveRecord::Base
	belongs_to :employee
	belongs_to :users
end
