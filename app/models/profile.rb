# == Schema Information
#
# Table name: profiles
#
#  id                 :integer          not null, primary key
#  add_info           :text
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  user_id            :integer
#

class Profile < ActiveRecord::Base
	belongs_to :employee
	belongs_to :users

	has_attached_file :image, :styles => { :large => "600x600>",:medium => "300x300>", :thumb => "150x150>" }
	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
end
