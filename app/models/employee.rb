# == Schema Information
#
# Table name: employees
#
#  id            :integer          not null, primary key
#  first_name    :string(255)
#  middle_name   :string(255)
#  last_name     :string(255)
#  email         :string(255)
#  age           :integer
#  date_of_birth :date
#  created_at    :datetime
#  updated_at    :datetime
#  user_id       :integer
#



class Employee < ActiveRecord::Base
	has_many :addresses
	has_many :educations
	has_many :tech_skills
	has_many :certificational
	belongs_to :users
end
