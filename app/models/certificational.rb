# == Schema Information
#
# Table name: certificationals
#
#  id                 :integer          not null, primary key
#  certification_name :string(255)
#  presentage         :integer
#  institute_name     :string(255)
#  start_date         :date
#  end_date           :date
#  user_id            :integer
#  created_at         :datetime
#  updated_at         :datetime
#

class Certificational < ActiveRecord::Base
	belongs_to :employee
	belongs_to :users
end
