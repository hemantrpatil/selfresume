# == Schema Information
#
# Table name: educations
#
#  id              :integer          not null, primary key
#  course          :string(255)
#  institute       :string(255)
#  university      :string(255)
#  year_of_passing :date
#  presentage      :integer
#  created_at      :datetime
#  updated_at      :datetime
#  employee_id     :integer
#  user_id         :integer
#

class Education < ActiveRecord::Base
	belongs_to :employee
	belongs_to :users
end
