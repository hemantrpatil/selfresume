class CreateTechSkills < ActiveRecord::Migration
  def change
    create_table :tech_skills do |t|
      t.string :skill_name
      t.integer :presentage
      t.string :institute_name
      t.date :start_date
      t.date :end_date
      

      t.timestamps
    end
  end
end
