class AddUserIdToAddressTable < ActiveRecord::Migration
  def change
  	add_column :addresses, :address_p, :string
      add_column :addresses, :city_p, :string
      add_column :addresses, :state_p, :string
      add_column :addresses, :zipcode_p, :string
      add_column :addresses, :mobileno_p, :string
      add_column :addresses, :landlineno_p, :string
      add_column :addresses, :user_id, :integer
      change_column :addresses, :zipcode, :string
  end
end
