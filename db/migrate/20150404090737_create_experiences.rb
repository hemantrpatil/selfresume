class CreateExperiences < ActiveRecord::Migration
  def change
    create_table :experiences do |t|
      t.string :companyname
      t.string :designation
      t.string :companyprofile
      t.string :duration

      t.timestamps
    end
  end
end
