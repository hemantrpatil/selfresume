class AddUserIdToTechSkill < ActiveRecord::Migration
  def change
    add_column :tech_skills, :user_id, :integer
  end
end
