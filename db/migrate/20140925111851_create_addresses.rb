class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :address
      t.string :city
      t.string :state
      t.integer :zipcode
      t.string :mobileno
      t.string :landlineno

      t.timestamps
    end
  end
end
