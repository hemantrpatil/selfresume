class CreateEducations < ActiveRecord::Migration
  def change
    create_table :educations do |t|
      t.string :course
      t.string :institute
      t.string :university
      t.date :year_of_passing
      t.integer :presentage

      t.timestamps
    end
  end
end
