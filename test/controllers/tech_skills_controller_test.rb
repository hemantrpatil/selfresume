require 'test_helper'

class TechSkillsControllerTest < ActionController::TestCase
  setup do
    @tech_skill = tech_skills(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tech_skills)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tech_skill" do
    assert_difference('TechSkill.count') do
      post :create, tech_skill: { end_date: @tech_skill.end_date, institute_name: @tech_skill.institute_name, presentage: @tech_skill.presentage, skill_name: @tech_skill.skill_name, start_date: @tech_skill.start_date, user_id: @tech_skill.user_id }
    end

    assert_redirected_to tech_skill_path(assigns(:tech_skill))
  end

  test "should show tech_skill" do
    get :show, id: @tech_skill
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tech_skill
    assert_response :success
  end

  test "should update tech_skill" do
    patch :update, id: @tech_skill, tech_skill: { end_date: @tech_skill.end_date, institute_name: @tech_skill.institute_name, presentage: @tech_skill.presentage, skill_name: @tech_skill.skill_name, start_date: @tech_skill.start_date, user_id: @tech_skill.user_id }
    assert_redirected_to tech_skill_path(assigns(:tech_skill))
  end

  test "should destroy tech_skill" do
    assert_difference('TechSkill.count', -1) do
      delete :destroy, id: @tech_skill
    end

    assert_redirected_to tech_skills_path
  end
end
