require 'test_helper'

class CertificationalsControllerTest < ActionController::TestCase
  setup do
    @certificational = certificationals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:certificationals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create certificational" do
    assert_difference('Certificational.count') do
      post :create, certificational: { certification_name: @certificational.certification_name, end_date: @certificational.end_date, institute_name: @certificational.institute_name, presentage: @certificational.presentage, start_date: @certificational.start_date, user_id: @certificational.user_id }
    end

    assert_redirected_to certificational_path(assigns(:certificational))
  end

  test "should show certificational" do
    get :show, id: @certificational
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @certificational
    assert_response :success
  end

  test "should update certificational" do
    patch :update, id: @certificational, certificational: { certification_name: @certificational.certification_name, end_date: @certificational.end_date, institute_name: @certificational.institute_name, presentage: @certificational.presentage, start_date: @certificational.start_date, user_id: @certificational.user_id }
    assert_redirected_to certificational_path(assigns(:certificational))
  end

  test "should destroy certificational" do
    assert_difference('Certificational.count', -1) do
      delete :destroy, id: @certificational
    end

    assert_redirected_to certificationals_path
  end
end
